package com.setsy.store.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    COMPLETED, PENDING, CANCELLED
}
