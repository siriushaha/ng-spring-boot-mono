/**
 * View Models used by Spring MVC REST controllers.
 */
package com.setsy.store.web.rest.vm;
