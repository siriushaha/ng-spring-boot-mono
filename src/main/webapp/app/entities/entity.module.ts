import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SetsyProductModule } from './product/product.module';
import { SetsyProductCategoryModule } from './product-category/product-category.module';
import { SetsyCustomerModule } from './customer/customer.module';
import { SetsyProductOrderModule } from './product-order/product-order.module';
import { SetsyOrderItemModule } from './order-item/order-item.module';
import { SetsyInvoiceModule } from './invoice/invoice.module';
import { SetsyShipmentModule } from './shipment/shipment.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        SetsyProductModule,
        SetsyProductCategoryModule,
        SetsyCustomerModule,
        SetsyProductOrderModule,
        SetsyOrderItemModule,
        SetsyInvoiceModule,
        SetsyShipmentModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SetsyEntityModule {}
