import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShipment } from 'app/shared/model/shipment.model';

@Component({
    selector: 'jhi-shipment-detail',
    templateUrl: './shipment-detail.component.html'
})
export class ShipmentDetailComponent implements OnInit {
    shipment: IShipment;

    constructor(private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.data.subscribe(({ shipment }) => {
            this.shipment = shipment.body ? shipment.body : shipment;
        });
    }

    previousState() {
        window.history.back();
    }
}
