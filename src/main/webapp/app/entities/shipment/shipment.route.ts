import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from 'app/core';
import { Shipment } from 'app/shared/model/shipment.model';
import { ShipmentService } from './shipment.service';
import { ShipmentComponent } from './shipment.component';
import { ShipmentDetailComponent } from './shipment-detail.component';
import { ShipmentUpdateComponent } from './shipment-update.component';
import { ShipmentDeletePopupComponent } from './shipment-delete-dialog.component';

@Injectable()
export class ShipmentResolvePagingParams implements Resolve<any> {
    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

@Injectable()
export class ShipmentResolve implements Resolve<any> {
    constructor(private service: ShipmentService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id);
        }
        return new Shipment();
    }
}

export const shipmentRoute: Routes = [
    {
        path: 'shipment',
        component: ShipmentComponent,
        resolve: {
            pagingParams: ShipmentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.shipment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment/:id/view',
        component: ShipmentDetailComponent,
        resolve: {
            shipment: ShipmentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.shipment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment/new',
        component: ShipmentUpdateComponent,
        resolve: {
            shipment: ShipmentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.shipment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment/:id/edit',
        component: ShipmentUpdateComponent,
        resolve: {
            shipment: ShipmentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.shipment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const shipmentPopupRoute: Routes = [
    {
        path: 'shipment/:id/delete',
        component: ShipmentDeletePopupComponent,
        resolve: {
            shipment: ShipmentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.shipment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
