import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SetsySharedModule } from 'app/shared';
import { SetsyAdminModule } from 'app/admin/admin.module';
import {
    CustomerService,
    CustomerComponent,
    CustomerDetailComponent,
    CustomerUpdateComponent,
    CustomerDeletePopupComponent,
    CustomerDeleteDialogComponent,
    customerRoute,
    customerPopupRoute,
    CustomerResolve,
    CustomerResolvePagingParams
} from './';

const ENTITY_STATES = [...customerRoute, ...customerPopupRoute];

@NgModule({
    imports: [SetsySharedModule, SetsyAdminModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CustomerComponent,
        CustomerDetailComponent,
        CustomerUpdateComponent,
        CustomerDeleteDialogComponent,
        CustomerDeletePopupComponent
    ],
    entryComponents: [CustomerComponent, CustomerUpdateComponent, CustomerDeleteDialogComponent, CustomerDeletePopupComponent],
    providers: [CustomerService, CustomerResolve, CustomerResolvePagingParams],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SetsyCustomerModule {}
