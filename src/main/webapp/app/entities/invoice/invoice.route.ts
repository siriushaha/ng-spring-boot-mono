import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from 'app/core';
import { Invoice } from 'app/shared/model/invoice.model';
import { InvoiceService } from './invoice.service';
import { InvoiceComponent } from './invoice.component';
import { InvoiceDetailComponent } from './invoice-detail.component';
import { InvoiceUpdateComponent } from './invoice-update.component';
import { InvoiceDeletePopupComponent } from './invoice-delete-dialog.component';

@Injectable()
export class InvoiceResolvePagingParams implements Resolve<any> {
    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

@Injectable()
export class InvoiceResolve implements Resolve<any> {
    constructor(private service: InvoiceService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id);
        }
        return new Invoice();
    }
}

export const invoiceRoute: Routes = [
    {
        path: 'invoice',
        component: InvoiceComponent,
        resolve: {
            pagingParams: InvoiceResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice/:id/view',
        component: InvoiceDetailComponent,
        resolve: {
            invoice: InvoiceResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice/new',
        component: InvoiceUpdateComponent,
        resolve: {
            invoice: InvoiceResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice/:id/edit',
        component: InvoiceUpdateComponent,
        resolve: {
            invoice: InvoiceResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const invoicePopupRoute: Routes = [
    {
        path: 'invoice/:id/delete',
        component: InvoiceDeletePopupComponent,
        resolve: {
            invoice: InvoiceResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
