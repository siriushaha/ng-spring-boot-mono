import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductOrder } from 'app/shared/model/product-order.model';

@Component({
    selector: 'jhi-product-order-detail',
    templateUrl: './product-order-detail.component.html'
})
export class ProductOrderDetailComponent implements OnInit {
    productOrder: IProductOrder;

    constructor(private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.data.subscribe(({ productOrder }) => {
            this.productOrder = productOrder.body ? productOrder.body : productOrder;
        });
    }

    previousState() {
        window.history.back();
    }
}
