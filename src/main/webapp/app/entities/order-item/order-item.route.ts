import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from 'app/core';
import { OrderItem } from 'app/shared/model/order-item.model';
import { OrderItemService } from './order-item.service';
import { OrderItemComponent } from './order-item.component';
import { OrderItemDetailComponent } from './order-item-detail.component';
import { OrderItemUpdateComponent } from './order-item-update.component';
import { OrderItemDeletePopupComponent } from './order-item-delete-dialog.component';

@Injectable()
export class OrderItemResolvePagingParams implements Resolve<any> {
    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

@Injectable()
export class OrderItemResolve implements Resolve<any> {
    constructor(private service: OrderItemService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id);
        }
        return new OrderItem();
    }
}

export const orderItemRoute: Routes = [
    {
        path: 'order-item',
        component: OrderItemComponent,
        resolve: {
            pagingParams: OrderItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.orderItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'order-item/:id/view',
        component: OrderItemDetailComponent,
        resolve: {
            orderItem: OrderItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.orderItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'order-item/new',
        component: OrderItemUpdateComponent,
        resolve: {
            orderItem: OrderItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.orderItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'order-item/:id/edit',
        component: OrderItemUpdateComponent,
        resolve: {
            orderItem: OrderItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.orderItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const orderItemPopupRoute: Routes = [
    {
        path: 'order-item/:id/delete',
        component: OrderItemDeletePopupComponent,
        resolve: {
            orderItem: OrderItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'setsyApp.orderItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
