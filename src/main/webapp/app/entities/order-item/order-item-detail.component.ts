import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrderItem } from 'app/shared/model/order-item.model';

@Component({
    selector: 'jhi-order-item-detail',
    templateUrl: './order-item-detail.component.html'
})
export class OrderItemDetailComponent implements OnInit {
    orderItem: IOrderItem;

    constructor(private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.data.subscribe(({ orderItem }) => {
            this.orderItem = orderItem.body ? orderItem.body : orderItem;
        });
    }

    previousState() {
        window.history.back();
    }
}
