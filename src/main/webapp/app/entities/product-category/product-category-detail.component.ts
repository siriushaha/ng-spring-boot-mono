import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductCategory } from 'app/shared/model/product-category.model';

@Component({
    selector: 'jhi-product-category-detail',
    templateUrl: './product-category-detail.component.html'
})
export class ProductCategoryDetailComponent implements OnInit {
    productCategory: IProductCategory;

    constructor(private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.data.subscribe(({ productCategory }) => {
            this.productCategory = productCategory.body ? productCategory.body : productCategory;
        });
    }

    previousState() {
        window.history.back();
    }
}
