import { Moment } from 'moment';
import { IOrderItem } from './order-item.model';
import { IInvoice } from './invoice.model';
import { ICustomer } from './customer.model';

export const enum OrderStatus {
    COMPLETED = 'COMPLETED',
    PENDING = 'PENDING',
    CANCELLED = 'CANCELLED'
}

export interface IProductOrder {
    id?: number;
    placedDate?: Moment;
    orderStatus?: OrderStatus;
    code?: string;
    orderItems?: IOrderItem[];
    invoices?: IInvoice[];
    customer?: ICustomer;
}

export class ProductOrder implements IProductOrder {
    constructor(
        public id?: number,
        public placedDate?: Moment,
        public orderStatus?: OrderStatus,
        public code?: string,
        public orderItems?: IOrderItem[],
        public invoices?: IInvoice[],
        public customer?: ICustomer
    ) {}
}
