import { Moment } from 'moment';
import { IShipment } from './shipment.model';
import { IProductOrder } from './product-order.model';

export const enum InvoiceStatus {
    PAID = 'PAID',
    ISSUED = 'ISSUED',
    CANCELLED = 'CANCELLED'
}

export const enum PaymentMethod {
    CREDIT_CARD = 'CREDIT_CARD',
    CASH_ON_DELIVERY = 'CASH_ON_DELIVERY',
    PAYPAL = 'PAYPAL'
}

export interface IInvoice {
    id?: number;
    date?: Moment;
    details?: string;
    invoiceStatus?: InvoiceStatus;
    paymentMethod?: PaymentMethod;
    paymentDate?: Moment;
    paymentAmount?: number;
    shipments?: IShipment[];
    order?: IProductOrder;
}

export class Invoice implements IInvoice {
    constructor(
        public id?: number,
        public date?: Moment,
        public details?: string,
        public invoiceStatus?: InvoiceStatus,
        public paymentMethod?: PaymentMethod,
        public paymentDate?: Moment,
        public paymentAmount?: number,
        public shipments?: IShipment[],
        public order?: IProductOrder
    ) {}
}
